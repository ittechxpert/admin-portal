import React from "react";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";

//Pages
import SignIn from "./pages/SignIn";
// import Dashboard from "./pages/Dashboard";
import PrivateRoute from "../src/Utils/PrivateRoute";
import Dashboard from "../src/Admin/Pages/Dashboard/index.js";

const App = () => (
  <BrowserRouter>
    <Switch>
      <Route exact path="/" component={SignIn} />
      {/* <Route exact path="/dashboard" component={Dashboard} /> */}
      <PrivateRoute exact path="/dashboard" component={Dashboard} />
      <Redirect to="/" />
    </Switch>
  </BrowserRouter>
);

export default App;
