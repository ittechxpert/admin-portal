// return the userId data from the session storage
export const getUser = () => {
  const userId = sessionStorage.getItem("userId");
  if (userId) return userId;
  else return null;
};
export const getID = () => {
  const ID = sessionStorage.getItem("ID");
   if (ID) return ID;
  console.log(ID, "id");
};

export const getRole = () => {
  const ID = sessionStorage.getItem("ID");
  if (ID) return ID;
  console.log(ID, "id");
};
  
// return the isLogged from the session storage
export const getToken = () => {
  return sessionStorage.getItem("isLogged") ? true : false;
};

// remove the isLogged and userId from the session storage
export const removeUserSession = () => {
  sessionStorage.removeItem("isLogged");
  sessionStorage.removeItem("userId");
  sessionStorage.removeItem("ID");
};

// set the isLogged and userId from the session storage
export const setUserSession = (isLogged, userId, ID) => {
  sessionStorage.setItem("isLogged", isLogged);
  sessionStorage.setItem("userId", userId);
  sessionStorage.setItem("ID", ID);
};

