import React from "react";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { removeUserSession, getUser, getID } from "../../../Utils/Common";
import { useHistory } from "react-router-dom";

const Topbar = () => {
  const user = getUser();
  console.log("userName =", user);
  let history = useHistory();

  const signoutFun = () => {
    toast.success("Logout Successfully...", {
      position: "top-right",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    });

    setTimeout(() => {
      removeUserSession();
      history.push("/admin");
    }, 3000);
  };

  return (
    <React.Fragment>
      <nav className="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
        <ToastContainer />

        <ul className="navbar-nav ml-auto">
          {/* <!-- Nav Item - Search Dropdown (Visible Only XS) --> */}
          <li className="nav-item dropdown no-arrow d-sm-none">
            <a
              className="nav-link dropdown-toggle"
              href="#"
              id="searchDropdown"
              role="button"
              data-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="false"
            >
              <i className="fas fa-search fa-fw"></i>
            </a>
            {/* <!-- Dropdown - Messages --> */}
            <div
              className="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in"
              aria-labelledby="searchDropdown"
            >
              <form className="form-inline mr-auto w-100 navbar-search">
                <div className="input-group">
                  <input
                    type="text"
                    className="form-control bg-light border-0 small"
                    placeholder="Search for..."
                    aria-label="Search"
                    aria-describedby="basic-addon2"
                  />

                  <div className="input-group-append">
                    <button className="btn btn-primary" type="button">
                      <i className="fas fa-search fa-sm"></i>
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </li>

          {/* <!-- Nav Item - User Information --> */}
          <li className="nav-item dropdown no-arrow">
            <a
              className="nav-link dropdown-toggle"
              href="#"
              id="userDropdown"
              role="button"
              data-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="false"
            >
              <span className="mr-2 d-none d-lg-inline text-gray-600">
                {user}
              </span>
              {/* <img
                className="img-profile rounded-circle"
                src="https://source.unsplash.com/QAB-WJcbgJk/60x60"
              /> */}
              <button
                type="button"
                className="btn btn-primary"
                href="javascript:void()"
                aria-expanded="false"
                onClick={signoutFun}
              >
                <i className="icon-logout" />
                <span className="nav-text">Sign out</span>
              </button>
            </a>
          </li>
          <li className="nav-item dropdown no-arrow">
            {/* <a
              className="ai-icon"
              href="javascript:void()"
              aria-expanded="false"
              onClick={signoutFun}
            >
              <i className="icon-logout" />
              <span className="nav-text">Sign out</span>
            </a> */}
            {/* <NavLink
              exact
              className="nav-link dropdown-toggle"
              className="ai-icon"
              to="/"
              aria-expanded="false"
              onClick={signoutFun}
            >
              <i className="icon-logout" />
              <span className="mr-2 d-none d-lg-inline text-gray-600 small">
                Sign out
              </span>
            </NavLink> */}
          </li>
        </ul>
      </nav>
    </React.Fragment>
  );
};

export default Topbar;
