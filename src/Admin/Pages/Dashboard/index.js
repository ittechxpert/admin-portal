import React, { useState, useEffect } from "react";
import "../Dashboard/index.css";
import Topbar from "../../../components/Navigation/Topbar";
// import PageHeading from "../../../components/PageHeading";
import { getID } from "../../../Utils/Common";
import axios from "axios";

const lists = [
  { id: 1, title: "Ex" },
  { id: 2, title: "M" },
  { id: 3, title: "I" },
  { id: 4, title: "B" },
  { id: 5, title: "A" },
  { id: 6, title: "NA" },
];

//getID
const ID = getID();
console.log("userID =", ID);

const Dashboard = () => {
  const [selected, setSelected] = useState(0);
  const URL = `https://stagingapi.kid-ex.com/api/assessor/getbyRole/${ID}/1`;
  const [_, setSelectedClient] = useState();

  //onChange value
  const [title, settitle] = useState("");
  const [maxAge, setmaxAge] = useState("");
  const [minAge, setminAge] = useState("");
  const [category, setcategory] = useState("");

  // api Object date
  const [option, setOption] = useState({});

  useEffect(() => {
    axios.get(URL).then((response) => {
      setOption(response.data.data.data);
      let res = response.data.data.data[0].activityDetails;
      settitle(res.title);
      setmaxAge(res.maxAge);
      setminAge(res.minAge);
      setcategory(res.category);
    });
  }, [URL]);

  const handleColor = (row) => {
    setSelected(row.id);
  };
  function handleSelectChange(event) {
    let selectedId = event.target.value;
    option.map((e) => {
      if (selectedId == e.id) {
        settitle(e.activityDetails.title);
        setmaxAge(e.activityDetails.maxAge);
        setminAge(e.activityDetails.minAge);
        setcategory(e.activityDetails.category);
      }
    });
    console.log("===", option);
    setSelectedClient(event.target.value);
  }
  return (
    <React.Fragment>
      <Topbar />
      <div id="wrapper">
        <div id="content-wrapper" className="d-flex flex-column">
          <div id="content">
            {/* <Topbar /> */}
            <div className="container">
              {/* <PageHeading title="Dashboard" /> */}
              <div className="row">
                <div className="col-lg-6">
                  <div className="d-flex">
                    <h5 className="pr-2 add_icon">{title} </h5>
                    <h5 className="pr-2 add_icon">{maxAge} </h5>
                    <h5 className="pr-2 add_icon">{minAge}</h5>
                    <h5 className="pr-2 add_icon">{category}</h5>
                  </div>
                  <img
                    src="https://image.freepik.com/free-photo/mand-holding-cup_1258-340.jpg"
                    alt=""
                    className="profileimg"
                    style={{ height: "", width: " 100%" }}
                  />
                </div>
                <div className="col-lg-6">
                  <h2>Grade</h2>
                  <div className="">
                    <div className="d-flex pb-3">
                      {lists.map((list, id) => (
                        <div className="grade-btn-wrapper" key={id}>
                          <button
                            className="btn login"
                            onClick={() => handleColor(list)}
                            style={{
                              backgroundColor:
                                list.id === selected ? "#007bff" : "",
                              width: "80px",
                            }}
                          >
                            {list.title}
                          </button>
                        </div>
                      ))}
                    </div>
                  </div>

                  <div className="input-group mb-3">
                    <select
                      defaultValue={"DEFAULT"}
                      className="custom-select"
                      id="inputGroupSelect01"
                      onChange={handleSelectChange}
                    >
                      {Object.entries(option).map(([keys, subject], i) => (
                        <option value={subject.activityDetails.id} key={i}>
                          {subject.activityDetails.title}
                        </option>
                      ))}
                    </select>
                  </div>
                  <div className="input-group mb-3">
                    <select
                      defaultValue={"DEFAULT"}
                      className="custom-select"
                      id="inputGroupSelect01"
                    >
                      <option value="DEFAULT">Choose...</option>
                      <option value="1">One</option>
                      <option value="2">Two</option>
                      <option value="3">Three</option>
                    </select>
                  </div>
                  <div className="mb-3">
                    <label
                      htmlFor="exampleFormControlInput1"
                      className="form-label"
                    >
                      Randam
                    </label>
                    <input
                      type="text"
                      className="form-control"
                      id="exampleFormControlInput1"
                      placeholder=""
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <footer className="sticky-footer bg-white">
            <div className="container my-auto">
              <div className="text-center my-auto pb-3">
                <button
                  type="button"
                  className="btn btn-primary "
                  style={{ width: "132px", marginRight: " 9px" }}
                >
                  Sumbit
                </button>
                <button
                  type="button"
                  className="btn btn-primary pl-2"
                  style={{ width: "132px" }}
                >
                  Skip
                </button>
              </div>
              <div className="copyright text-center">
                <span>Copyright &copy; Your Website 2019</span>
              </div>
            </div>
          </footer>
        </div>
      </div>
      <a className="scroll-to-top rounded" href="">
        <i className="fas fa-angle-up"></i>
      </a>
    </React.Fragment>
  );
};

export default Dashboard;
