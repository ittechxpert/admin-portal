import React, { useState } from "react";
import { NavLink } from "react-router-dom";
import { useHistory } from "react-router-dom";
import axios from "axios";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { setUserSession, getToken } from "../../Utils/Common";

const SignIn = () => {
  let history = useHistory();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  // const [val, setvalue] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    axios
      .post("https://stagingapi.kid-ex.com/api/assessor/login", {
        email: email,
        password: password,
      })
      .then(function (response) {
        if (response.data.status === true) {
          console.log("response", response);
          console.log(response.data.data.personName);

          toast.success("Login Successfully...", {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
          setUserSession(
            true,
            response.data.data.personName,
            response.data.data.id
          );

          setTimeout(() => {
            history.push("/dashboard");
          }, 2000);
        }
        return response;
      })
      .catch(function () {
        toast.error("Wrong Email and password", {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      });
  };
  // col-xl-6 col-lg-12 col-md-10
  return (
    <React.Fragment>
      <ToastContainer />
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-xl-6 col-lg-12 col-md-10">
            <div className="card o-hidden border-0 shadow-lg my-5">
              <div className="card-body p-0">
                <div className="row">
                  {/* <div className="col-lg-6 d-none d-lg-block bg-login-image"></div> */}
                  <div className="col-lg-12">
                    <div className="p-5">
                      <div className="text-center">
                        <h1 className="h4 text-gray-900 mb-4">Welcome Back!</h1>
                      </div>

                      {getToken() === true ? (
                        <li>
                          <NavLink
                            className="nav-link scrollto"
                            to="/dashboard"
                          >
                            Dashboard
                          </NavLink>
                        </li>
                      ) : (
                        <>
                          <form onSubmit={handleSubmit} className="user">
                            <div className="form-group">
                              <input
                                type="email"
                                className="form-control form-control-user"
                                id="exampleInputEmail"
                                aria-describedby="emailHelp"
                                placeholder="Enter Email Address..."
                                value={email}
                                onChange={(e) => setEmail(e.target.value)}
                              />
                            </div>
                            <div className="form-group">
                              <input
                                type="password"
                                className="form-control form-control-user"
                                id="exampleInputPassword"
                                placeholder="Password"
                                value={password}
                                onChange={(e) => setPassword(e.target.value)}
                              />
                            </div>
                            <div className="form-group">
                              <div className="custom-control custom-checkbox small">
                                <input
                                  type="checkbox"
                                  className="custom-control-input"
                                  id="customCheck"
                                />
                                <label
                                  className="custom-control-label"
                                  htmlFor="customCheck"
                                >
                                  Remember Me
                                </label>
                              </div>
                            </div>
                            <button
                              type="submit"
                              className="btn btn-primary btn-user btn-block"
                            >
                              Login
                            </button>
                          </form>
                          <hr />
                          <div className="text-center">
                            <a className="small" href="forgot-password.html">
                              Forgot Password?
                            </a>
                          </div>
                          <div className="text-center">
                            <NavLink className="small" to="/signup">
                              Create an Account!
                            </NavLink>
                          </div>
                        </>
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default SignIn;
